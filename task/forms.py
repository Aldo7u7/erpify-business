from django.forms import ModelForm
from django import forms
from .models import Task, Producto, Cliente, Produccion, Ventas, Compras, H_R, Mantenimiento, Evento
from .models import Product, Humanos, Sales, piloto, proyectos, Campania, Medio, Nomina

class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields =['title', 'description', 'important']
        
class ProductoForm(ModelForm):
    class Meta:
        model = Producto
        fields = ['nombre','ingreso','gasto']
        
class ClientesCRMfrom(ModelForm):
    class Meta:
        model = Cliente
        fields = ['id','nombre','apellido1','apellido2','numero','correo','ventas_realizadas','cliente_importante']
        
class ProduccionForm(forms.ModelForm):
    class Meta:
        model = Produccion
        fields = ['nombre', 'modelo', 'serie', 'NumeroOrden', 'cantidad']
        
class VentasForm(forms.ModelForm):
    class Meta:
        model = Ventas
        fields = ['account','name','rep','manager','product','quantity','price','status']
        
class ComprasForm(forms.ModelForm):
    class Meta:
        model = Compras
        fields = ['nombre', 'modelo', 'serie', 'NumeroOrden', 'cantidad', 'FechaCompra']
        
class HRForm(forms.ModelForm):
    class Meta:
        model = H_R
        fields = ['NumeroEmpleado', 'nombres', 'PrimerApellido', 'SegundoApellido', 'puesto', 'RFC', 'FechaIngreso']
        
class MantenimientoForm(ModelForm):
    class Meta:
        model = Mantenimiento
        fields = ['equipo', 'linea', 'tecnico', 'numero_empleado', 'fecha']

class EventoForm(forms.ModelForm):
    class Meta:
        model = Evento
        fields = ['nombre', 'descripcion', 'sala', 'fecha']
        
class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['id', 'nombre', 'modelo', 'precio','cantidad']
        
class HumanosForm(forms.ModelForm):
    class Meta:
        model = Humanos
        fields =['NumeroEmpleado','Nombres','Apellido1','Apellido2','RFC','Puesto','fechaIngreso'] 

class SalesForm(forms.ModelForm):
    cliente = forms.ModelChoiceField(queryset=Cliente.objects.all(), 
                                    label="Cliente",
                                    widget=forms.Select(attrs={'class': 'form-control'}))  # Personaliza la apariencia
    producto = forms.ModelChoiceField(queryset=Product.objects.all(), 
                                    label="Producto",
                                    widget=forms.Select(attrs={'class': 'form-control'}))
    class Meta:
        model = Sales
        fields = ['cliente', 'producto', 'fecha_venta', 'cantidad_vendida']
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['cliente'].label_from_instance = lambda obj: f"{obj.id} - {obj.nombre} {obj.apellido1}"
        self.fields['producto'].label_from_instance = lambda obj: f"{obj.id} - {obj.nombre}" 

class PilotoForm(forms.ModelForm):
    class Meta:
        model = piloto
        fields = ['nombre', 'descripcion', 'linea']
        
class ProyectoForm(forms.ModelForm):
    empleado = forms.ModelChoiceField(queryset=Humanos.objects.all())  # Campo para seleccionar el empleado
    class Meta:
        model = proyectos
        fields = ['nombre', 'descripcion', 'empleado']  # Campos del formulario

class CampaniaForm(forms.ModelForm):
    medio = forms.ModelChoiceField(queryset=Medio.objects.all(), widget=forms.RadioSelect)
    class Meta:
        model = Campania
        fields = ['nombre', 'descripcion', 'fecha_inicio', 'presupuesto', 'medio']

class NominaForm(forms.ModelForm):
    empleado = forms.ModelChoiceField(queryset=Humanos.objects.all())  # Campo para seleccionar humanos
    class Meta:
        model = Nomina
        fields = ['empleado', 'salario_diario', 'fecha_inicio']