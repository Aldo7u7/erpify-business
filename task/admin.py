from django.contrib import admin
from .models import Task, Producto, Cliente, Produccion, Ventas, Compras, H_R, Evento, Sala, Nomina
from .models import Mantenimiento, Product, Humanos, Sales,piloto,linea, proyectos, Campania, Medio
#aqui en este codigo, se agregan las tablas de las bases de datos
#que creamos en models.py para que el admin tenga acceso a ellas.

#clase para mostrar la fecha de creacion de la tarea, desde el admin de Django.
class TaskAdmin(admin.ModelAdmin):
    readonly_fields = ("created",)

#creacion de administrador de tareas del admin de Django.
admin.site.register(Task, TaskAdmin)
admin.site.register(Producto)
admin.site.register(Cliente)
admin.site.register(Produccion)
admin.site.register(Ventas)
admin.site.register(Compras)
admin.site.register(Nomina)
admin.site.register(Mantenimiento)
admin.site.register(Evento)
admin.site.register(Sala)
admin.site.register(Product)
admin.site.register(Humanos)
admin.site.register(Sales)
admin.site.register(piloto)
admin.site.register(linea)
admin.site.register(proyectos)
admin.site.register(Medio)
admin.site.register(Campania)