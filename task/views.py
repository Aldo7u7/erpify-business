from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from .forms import TaskForm, ClientesCRMfrom, ProduccionForm, ComprasForm
from .models import Task, Cliente, Produccion, Compras
from django.utils import timezone
from django.contrib import messages

# AQUI SE CREAN LOS VIEWS BASICOS DE LA PAGINA WEB:
def home(request):
    return render(request, 'home.html')

def signup(request):
    if request.method == 'GET':
        return render(request, 'signup.html',{
         'form': UserCreationForm
        })
    else:
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.create_user(username=request.POST['username'],password=request.POST['password1'])
                user.save()
                login(request, user)
                return redirect('tasks')
            except IntegrityError:
               return render(request,'signup.html',{
                'form': UserCreationForm,
                "error": 'User already Exist'
               })
            
        return render(request, 'signup.html',{
                'form': UserCreationForm,
                "error": 'User already Exists'
        })
        
@login_required        
def tasks(request):
    tasks = Task.objects.filter(user=request.user, datecompleted__isnull=True)
    return render(request,'tasks.html',{'tasks': tasks}) #funcion task

@login_required
def tasks_completed(request):
    tasks = Task.objects.filter(user=request.user, datecompleted__isnull=False).order_by('-datecompleted')
    return render(request,'tasks.html',{'tasks': tasks}) #funcion task

@login_required
def create_task(request): #funcion para create_task
  if request.method == 'GET':
        return render(request, 'create_task.html',{
        'form': TaskForm
        })
  else:
      try:
          form = TaskForm(request.POST)
          new_task = form.save(commit=False)
          new_task.user = request.user
          new_task.save()
          return redirect('tasks')
      except ValueError:
          return render(request, 'create_task.html',{
              'form': TaskForm,
              'error': 'Please provide valid data' 
          })
          
@login_required          
def task_detail(request, task_id):
    if request.method == 'GET':
        task = get_object_or_404(Task, pk=task_id, user=request.user)
        form = TaskForm(instance=task)
        return render(request, 'task_detail.html', {'task' : task, 'form' : form})
    else:
       try:
          task = get_object_or_404(Task, pk=task_id, user=request.user)
          form = TaskForm(request.POST, instance=task)
          form.save()
          return redirect('tasks')
       except ValueError:
           return render(request, 'task_detail.html', {'task':task, 'form':form, 'error': "Error updating task"})
      
@login_required           
def complete_task(request, task_id):
    task = get_object_or_404(Task, pk=task_id, user=request.user)
    if request.method == 'POST':
        task.delete()
        return redirect('tasks')
  
@login_required  
def delete_task(request, task_id):
    task = get_object_or_404(Task, pk=task_id, user=request.user)
    if request.method == 'POST':
        task.datecompleted = timezone.now()
        task.save()
        return redirect('home')
          
@login_required   
def signout(request):
    logout(request)
    return redirect('home')

def signin(request):
    if request.method == 'GET':
       return render(request, 'signin.html',{
       'form': AuthenticationForm
       })
    else:
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is None:
            return render(request, 'signin.html',{
                'form': AuthenticationForm,
                'error': 'Username or password is incorrect'
            })
        else:
            login(request,user)
            return redirect('tasks')
        
#APARTIR DE AQUI SE CREAN LOS MODULOS ESPECIFICOS DE LA PAGINA WEB:

import json
@login_required
def finance(request):
    # ... (código para obtener datos desde tu modelo) ...
    labels = ['March', 'April', 'May']  # Ejemplo de labels
    data = [12000, 19000, 3000]  # Ejemplo de data
    context = {
        'labels': json.dumps(labels),  # Convertir a JSON
        'data': json.dumps(data),      # Convertir a JSON
    }
    return render(request, 'finance.html', context)

from .forms import EventoForm
from .models import Evento
@login_required
def events(request):
    if request.method == 'POST':
        form = EventoForm(request.POST)
        if form.is_valid():
            eventos = form.save()
            eventos.refresh_from_db()
            return redirect('events')
    else: 
        form = EventoForm()
    eventos = Evento.objects.all()
    return render(request, 'events.html', {'form': form, 'eventos': eventos})

@login_required
def production(request):
    if request.method == 'POST':
        form = ProduccionForm(request.POST)
        if form.is_valid():
            produccion = form.save()  # Omite el argumento 'id'
            # Actualizar el objeto con datos de la base de datos
            produccion.refresh_from_db()
            return redirect('production')
    else:  # Solicitud GET
        form = ProduccionForm()  # Instanciar el formulario
        produccion = Produccion.objects.all()  # Obtener todas las órdenes de producción
    return render(request, 'production.html', {'form': form, 'produccion': produccion})

from .forms import SalesForm
from .models import Sales
@login_required
def sales(request):
    if request.method == 'POST':
        form = SalesForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('sales') 
    else:
        form = SalesForm()
    ventas = Sales.objects.all()  # Obtenemos todas las ventas
    return render(request, 'sales.html', {'form': form, 'ventas': ventas})  

from .forms import HumanosForm
from .models import Humanos
@login_required
def HR(request):
    if request.method == 'POST':
        form = HumanosForm(request.POST)
        if form.is_valid():
            form.save()  # Guarda directamente, no es necesario refresh_from_db()
            return redirect('HR')  # Redirige para evitar reenvío del formulario
    else:
        form = HumanosForm()
    humanos = Humanos.objects.all()  # Corregido nombre de la variable
    return render(request, 'HR.html', {'form': form, 'humanos': humanos}) 

@login_required
def purchasing(request):
    if request.method == 'POST':
        form = ComprasForm(request.POST)
        if form.is_valid():
            compra = form.save()  # Omite el argumento 'id'
            # Actualizar el objeto con datos de la base de datos
            compra.refresh_from_db()
            return redirect('purchasing')
    else:  # Solicitud GET
        form = ComprasForm()  # Instanciar el formulario
        compras = Compras.objects.all()  # Obtener todas las órdenes de compras
        return render(request, 'purchasing.html', {'form': form, 'compras': compras})

from .forms import MantenimientoForm
from .models import Mantenimiento
@login_required
def maintenance(request):
    if request.method == 'POST':
        form = MantenimientoForm(request.POST)
        if form.is_valid():
            mantenimiento = form.save()
            mantenimiento.refresh_from_db()
            return redirect('maintenance')
    else:  # Agregar esta rama para manejar la solicitud GET
        form = MantenimientoForm()
    mantenimientos = Mantenimiento.objects.all()
    return render(request, 'maintenance.html', {'form': form, 'mantenimientos': mantenimientos})
    
@login_required
def CRM(request):
    if request.method == 'POST':
        form = ClientesCRMfrom(request.POST)
        if form.is_valid():
            # Save the new client to the database
            form.save()
            messages.success(request, 'Client successfully saved!')
            # Redirect back to the CRM page after successful saving
            return redirect('/CRM/')
        else:
            messages.error(request, 'There was an error saving the client!')
    else:
        # Initialize an empty form for adding new clients
        form = ClientesCRMfrom()
    # Get all existing clients to display in the list
    clientes = Cliente.objects.all()
    # Render the CRM template with the form and list of clients
    return render(request, 'CRM.html', {
        'form': form,
        'clientes': clientes
    })

from .forms import ProductForm
from .models import Product
@login_required
def stock(request):
    if request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            products = form.save()
            products.refresh_from_db()
            return redirect('stock')
    else:
        form = ProductForm()
    products = Product.objects.all()
    return render(request, 'stock.html', {'form': form, 'products': products})

from .forms import PilotoForm
from .models import piloto
@login_required
def quality(request):
    if request.method == 'POST':
        form = PilotoForm(request.POST)
        if form.is_valid():
            pilotos = form.save()
            pilotos.refresh_from_db()
            return redirect('quality')
    else: 
        form = PilotoForm()
    pilotos = piloto.objects.all()
    return render(request, 'quality.html', {'form': form, 'pilotos': pilotos})

from .forms import NominaForm
from .models import Nomina
@login_required
def payroll(request):
    nominas = Nomina.objects.all()
    form = NominaForm()
    if request.method == 'POST':
        form = NominaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('payroll') 
    return render(request, 'payroll.html', {  
        'nominas': nominas,
        'form': form,
    })

from .forms import ProyectoForm
from .models import proyectos
@login_required
def Projects(request):
    proyectos_list = proyectos.objects.all()
    form = ProyectoForm()
    if request.method == 'POST':
        form = ProyectoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('Projects') 
    return render(request, 'Projects.html', {  
        'proyectos': proyectos_list,
        'form': form,
    })

from .forms import CampaniaForm
from .models import Campania
@login_required
def marketing(request):
    campañas = Campania.objects.all()
    form = CampaniaForm()
    if request.method == 'POST':
        form = CampaniaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('marketing') 
    return render(request, 'marketing.html', {  
        'Campania': campañas,
        'form': form,
    })